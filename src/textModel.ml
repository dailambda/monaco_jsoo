open Js_of_ocaml
open Js
open Unsafe
open Jstools

type _t
type t = _t Js.t

let getValue : (* ?eol:bool -> ?preserveBOM:bool -> *) t -> js_string_t =
  fun (* ?eol ?preserveBOM *) t ->
  meth_call t "getValue" [| |]

let setValue : t -> js_string_t -> unit = fun t s ->
  meth_call t "setValue" [| inject s |]

let deltaDecorations
    ~old:(old: Decoration.Id.t array)
    ~new_:(new_: Decoration.deltaDecoration_t array)
    ?ownerId
    t =
  let old  = Js.array old in
  let new_ = Js.array new_ in
  let jss =
    match ownerId with
    | None ->
        meth_call t "deltaDecorations" [| inject old; inject new_ |]
    | Some (id : int) -> (* Somehow it is number, though the editor id is a string *)
        meth_call t "deltaDecorations" [| inject old; inject new_; inject id |]
  in
  Array.map Decoration.Id.of_js_string_t @@ Js.to_array jss
