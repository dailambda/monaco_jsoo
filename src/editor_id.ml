open Js_of_ocaml

type _t = Js.js_string
type t = _t Js.t
let to_string = Js.to_string
let of_string = Js.string
