(** IDs used in Monaco *)

open Js_of_ocaml

module type S = sig
  type _t
  type t = _t Js.t
  val to_string : t -> string
  val to_js_string_t : t -> Js.js_string Js.t
  val of_string : string -> t
  val of_js_string_t : Js.js_string Js.t -> t
end

module Make() : S
