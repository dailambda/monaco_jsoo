open Js_of_ocaml
open Js

(* https://microsoft.github.io/monaco-editor/api/interfaces/monaco.IRange.html *)
class type _t = object
  method startLineNumber : int readonly_prop
  method startColumn : int readonly_prop
  method endLineNumber : int readonly_prop
  method endColumn : int readonly_prop
end

type t = _t Js.t

let create (sln : int) (scl : int) (eln : int) (ecl : int) : t =
  let open Unsafe in
  new_obj (pure_js_expr "monaco.Range") [| inject sln; inject scl; inject eln; inject ecl |]
