open Js_of_ocaml
open Js

(** https://microsoft.github.io/monaco-editor/api/interfaces/monaco.IRange.html *)
class type _t = object
  method startLineNumber : int readonly_prop
  method startColumn : int readonly_prop
  method endLineNumber : int readonly_prop
  method endColumn : int readonly_prop
end

type t = _t Js.t

val create : int -> int -> int -> int -> t
