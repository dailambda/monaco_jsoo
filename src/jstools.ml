open Js_of_ocaml
open Js

module Html = Dom_html

let from_Some = function
  | Some x -> x
  | _ -> assert false

let (!$) = Js.string

module Console = struct
  let log a = Firebug.console##log a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

let getById coerce s =
  match Html.getElementById_coerce s coerce with
  | Some x -> x
  | None ->
      Console.logf "Element %s is not found" s;
      exit 1

let alert s = Html.window##alert !$s

let is_worker () =
  try ignore (Js.Unsafe.eval_string "window"); false with _ -> true

type js_string_t = js_string t
type 'a js_array_t = 'a js_array t
type bool_t = bool t

let pp_as_json ppf x = Format.fprintf ppf "%s" (Js.to_string (_JSON##stringify x))
