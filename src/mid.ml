open Js_of_ocaml

module type S = sig
  type _t
  type t = _t Js.t
  val to_string : t -> string
  val to_js_string_t : t -> Js.js_string Js.t
  val of_string : string -> t
  val of_js_string_t : Js.js_string Js.t -> t
end

module Make() = struct
  type _t = Js.js_string
  type t = _t Js.t
  let to_string = Js.to_string
  let to_js_string_t x = x
  let of_string = Js.string
  let of_js_string_t x = x
end
