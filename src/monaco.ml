open Js_of_ocaml
open Js
open Unsafe
open Jstools

let run dir (* directory with loader.js *) (cb : unit -> unit) : unit =
  (* See https://requirejs.org/docs/api.html *)
  (* require.config({ paths: { vs: 'dire/cto/ry' } }); *)
  Console.log dir;
  ((meth_call (eval_string "require") "config"
      [| inject @@ object%js val paths = object%js val vs = Js.string dir end end |]) : unit);
  (* require(['vs/editor/editor.main'], cb); *)
  ((fun_call (eval_string "require")
             [| inject @@ array [| Js.string "vs/editor/editor.main" |];
                inject cb |]): unit)
