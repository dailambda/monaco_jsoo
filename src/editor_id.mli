open Js_of_ocaml

type _t
type t = _t Js.t

val to_string : t -> string

val of_string : string -> t
