open Js_of_ocaml
open Jstools

type _t
type t = _t Js.t
val getValue : t -> js_string_t
val setValue : t -> js_string_t -> unit

val deltaDecorations :
  old: Decoration.Id.t array ->
  new_: Decoration.deltaDecoration_t array ->
  ?ownerId: int (* probably the number found in the Editor_id.t *) ->
  t ->
  Decoration.Id.t array
