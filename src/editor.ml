open Js_of_ocaml
open Js
open Unsafe
open Jstools

type _t
type t = _t Js.t

module Option = struct
  open Moption

  (** classes of options *)

  type globalEditor = [ `GlobalEditor ]
  type editor = [ `Editor ]
  type editorConstruction = [ editor | `EditorConstruction ]
  type standaloneEditorConstruction = [ globalEditor | editorConstruction ]
  type diffEditor = [ editor | `DiffEditor ]
  type diffEditorConstruction = [ diffEditor | `DiffEditorConstruction ]
  type standaloneDiffEditorConstruction = [ diffEditorConstruction | `StandaloneDiffEditorConstruction ]

  let readOnly b : [> editor] t = bool "readOnly" b
  let language (lang : Languages.t) : [> standaloneEditorConstruction] t = any "language" lang
  let theme s : [> globalEditor] t = string "theme" s
  let value s : [> standaloneEditorConstruction] t = string "value" s
  let automaticLayout b : [> editor] t = bool "automaticLayout" b
end

module Id = Mid.Make()

let editor () = eval_string "monaco.editor"

let create elem (opts : [< Option.standaloneEditorConstruction ] Moption.t array) =
  meth_call (editor ()) "create" [| inject elem; inject @@ Moption.options opts |]

let updateOptions t (opts : [< Option.editor | Option.globalEditor ] Moption.t array) =
  meth_call t "updateOptions" [| inject @@ Moption.options opts |]

let getModel t = meth_call t "getModel" [| |]

let getId t = Id.of_js_string_t @@ meth_call t "getId" [| |]

let deltaDecorations
    ~old:(old: Decoration.Id.t array)
    ~new_:(new_: Decoration.deltaDecoration_t array)
    t =
  let old : Decoration.Id.t js_array_t = Js.array old in
  let new_ : Decoration.deltaDecoration_t js_array_t = Js.array new_ in
  let jss = meth_call t "deltaDecorations" [| inject old; inject new_ |] in
  Array.map Decoration.Id.of_js_string_t @@ Js.to_array jss
