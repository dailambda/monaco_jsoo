open Js_of_ocaml
open Js

module Option = struct
  include Moption

  type modelDecoration = [ `ModelDecoration ]

  let isWholeLine b : [> modelDecoration] t = bool "isWholeLine" b
  let linesDecorationsClassName s : [> modelDecoration] t = string "linesDecorationsClassName" s
  let inlineClassName s : [> modelDecoration] t = string "inlineClassName" s

  (* not imlemented
  after
  afterContentClassName
  before
  beforeContentClassName
  className
  firstLineDecorationClassName
  glyphMarginClassName
  glyphMarginHoverMessage
  hoverMessage
  inlineClassNameAffectsLetterSpacing
  *)
end

module Id = Mid.Make()

class type deltaDecoration = object
  method options : Option.modelDecoration Option.options readonly_prop
  method range : Range.t readonly_prop
end

type deltaDecoration_t = deltaDecoration Js.t

let deltaDecoration (range : Range.t) (opts : Option.modelDecoration Moption.t array) =
  object%js
    val range = range
    val options = Moption.options opts
  end
