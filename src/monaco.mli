(** [run dir f] loads Monaco editor module placed at [dir] directory then runs
    the callback function [f].  Functions defined in this library must be
    executed inside this callback to access [monaco] JS object.

    The directory [dir] must contain ["loader.js"] file of "min" version of
    Monaco editor.

    Example:

    - Expand the Monaco editor package found at
        https://registry.npmjs.org/monaco-editor/-/monaco-editor-0.33.0.tgz
      and have package/ directory.

    - An HTML file with:
       <script type="text/javascript" src="package/min/vs/loader.js"></script>
       <script type="text/javascript" defer="defer" src="xxx.bc.js"></script>

    - A compiled JS file xxx.bc.js which calls

       [Monaco_jsoo.run "package/min/vs" f]
*)
val run : string -> (unit -> unit) -> unit
