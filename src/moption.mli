(** Builders of options used in Monaco *)

(** The type of option.  The phantom parameter ['cls] is used to express
    the subtyping of the options. *)
type 'cls t

(** Set of options *)
type 'cls options

val bool : string -> bool -> 'cls t
val string : string -> string -> 'cls t
val any : string -> 'a -> 'cls t
val options : 'cls t array -> 'cls options
