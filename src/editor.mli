open Js_of_ocaml

type _t

(** Editor type *)
type t = _t Js.t

(** Options for the editor functions and methods.

    See the API document for each option semantics:
    https://microsoft.github.io/monaco-editor/api/modules/monaco.editor.html
*)
module Option : sig
  open Moption

  (** Option classe hierarchy *)

  type globalEditor = [ `GlobalEditor ]
  type editor = [ `Editor ]
  type editorConstruction = [ editor | `EditorConstruction ]
  type standaloneEditorConstruction = [ globalEditor | editorConstruction ]
  type diffEditor = [ editor | `DiffEditor ]
  type diffEditorConstruction = [ diffEditor | `DiffEditorConstruction ]
  type standaloneDiffEditorConstruction = [ diffEditorConstruction | `StandaloneDiffEditorConstruction ]

  (* Only few options are defined now but it is easy to add your own *)

  val readOnly : bool -> [> editor] t

  val language : Languages.t -> [> standaloneEditorConstruction] t

  (** Option to set the theme of an editor.

      Currently setting the theme of 1 editor affects the other editors
      in the same browser window.  See:
      https://github.com/Microsoft/monaco-editor/issues/338 *)
  val theme : string -> [> globalEditor] t

  val value : string -> [> standaloneEditorConstruction] t

  val automaticLayout : bool -> [> editor] t
end

module Id : Mid.S

(** Calls [monaco.editor.create] to create an editor over the given DOM
    element. *)
val create : Dom_html.element Js.t -> [< Option.standaloneEditorConstruction ] Moption.t array -> t

(** Call [updateOptions] method to update options of the editor *)
val updateOptions : t -> [< Option.editor | Option.globalEditor ] Moption.t array -> unit

(** [getModel t] returns the text model of the editor [t] *)
val getModel : t -> TextModel.t

(** https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.IEditor.html#getId *)
val getId : t -> Id.t

(** https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.ICodeEditor.html#deltaDecorations *)
val deltaDecorations : old:Decoration.Id.t array -> new_:Decoration.deltaDecoration_t array -> t -> Decoration.Id.t array
