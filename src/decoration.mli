open Js_of_ocaml
open Js

(** https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.IModelDecorationOptions.html *)
module Option : sig
  type modelDecoration = [ `ModelDecoration ]

  val isWholeLine : bool -> [> modelDecoration] Moption.t
  val linesDecorationsClassName : string -> [> modelDecoration] Moption.t
  val inlineClassName : string -> [> modelDecoration] Moption.t
end

module Id : Mid.S

(** https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.IModelDeltaDecoration.html *)
class type deltaDecoration = object
  method options : Option.modelDecoration Moption.options readonly_prop
  method range : Range.t readonly_prop
end

type deltaDecoration_t = deltaDecoration Js.t

val deltaDecoration : Range.t -> Option.modelDecoration Moption.t array -> deltaDecoration_t
