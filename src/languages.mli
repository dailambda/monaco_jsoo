open Js_of_ocaml
open Js

type _t
type t = _t Js.t

(** To get an already registered language *)
val of_string : string -> t

val to_string : t -> string

(** [register n] adds a new language named [n] and returns its id. *)
val register : string -> t

(** [setMonarchTokensProvider t o] sets Monarch Token Provider of
    the language [t].  The provider [o] is a JS object documented
    at https://microsoft.github.io/monaco-editor/monarch.html
*)
val setMonarchTokensProvider : t -> Unsafe.any -> unit
