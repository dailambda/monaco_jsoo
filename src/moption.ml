open Js_of_ocaml
open Js
open Unsafe
open Jstools

type 'cls t = string * any
type 'cls options
let bool n b = n, inject @@ Js.bool b
let string n s = n, inject @@ !$ s
let any n a = n, inject a
let options = obj
