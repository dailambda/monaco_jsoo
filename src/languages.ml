open Js_of_ocaml
open Js
open Unsafe

type _t = js_string
type t = _t Js.t

let languages () = eval_string "monaco.languages"

let of_string = Js.string

let to_string = Js.to_string

let register (* options... *) id =
  let t = of_string id in
  let ep = obj [| "id", inject t |] in
  (meth_call (languages ()) "register" [| ep |] : unit);
  t

let setMonarchTokensProvider t json =
  meth_call (languages ()) "setMonarchTokensProvider"
    [| inject t; inject json |]
