open Js_of_ocaml
open Js
open Monaco_jsoo
open Jstools

let () =
  Monaco.run "package/min/vs" @@ fun () ->

  (* monaco is only bound in the callback *)
  Console.log (Unsafe.eval_string "monaco");

  (* Editor.create *)
  let c = Html.getElementById "container" in
  let e = Editor.(create c
                    Option.[| (* language (Languages.of_string "javascript"); *)
                              readOnly false;
                              theme "vs-dark";
                              value "// Hello world\n// javascript\n// here we are\n"
                           |])
  in

  (* Editor.updateOptions *)
  let theme_change = Html.getElementById "theme_change" in
  theme_change##.onclick := Html.handler (fun _ ->
      Editor.(updateOptions e Option.[| theme "vs" |]);
      Js._false);

  (* Editor.getModel *)
  let model = Editor.getModel e in

  (* TextModel.getValue and setValue *)
  let report fmt =
    Format.kasprintf (fun s ->
        TextModel.(setValue model ((getValue model)##concat (Js.string s)))
      ) fmt
  in

  (* Editor.getId *)
  let id = Editor.getId e in
  report "Editor.getId: %s@." (Editor.Id.to_string id);

  (* Range.create *)
  let r = Range.create 1 1 3 3 in
  report "Range.create: %a@." pp_as_json r;

  (* TextModel.deltaDecorations *)
  let add_decoration = Html.getElementById "add_decoration" in
  add_decoration##.onclick := Html.handler (fun _ ->
      let _newds =
        TextModel.deltaDecorations
          ~old: [| |]
          ~new_:
            [|
                Decoration.deltaDecoration
                  (Range.create 2 1 2 10)
                  Decoration.Option.[| inlineClassName "myInlineDecoration" |]
            |]
          model
      in
      (* It seems setValue clear the decoration. *)
      (* report "Editor.deltaDecorations: %a@." pp_as_json newds; *)
      Js._false);
  ()
