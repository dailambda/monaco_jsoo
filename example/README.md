Expand monaco-editor-0.33.0.tgz (or later) to have package/ directory here:

```
$ wget https://registry.npmjs.org/monaco-editor/-/monaco-editor-0.33.0.tgz
$ tar zxvf monaco-editor-0.33.0.tgz
```

