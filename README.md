# Monaco_jsoo

OCaml Js_of_ocaml interface for [Monaco editor library](https://microsoft.github.io/monaco-editor/).

You need monaco-editor JS package 0.33.0 or later: https://registry.npmjs.org/monaco-editor/-/monaco-editor-0.33.0.tgz
